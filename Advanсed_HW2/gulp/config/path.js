//ім'я теки проєкту
import * as nodePath from 'path';

const rootFolder = nodePath.basename(nodePath.resolve())

//шляхи тек
const buildFolder = `./dist`;
const srcFolder = `./src`;

export const path = {
    build: {
        html: `${buildFolder}`,
        css: `${buildFolder}/style/`,
        js:`${buildFolder}/js/`,
        images:`${buildFolder}/image/`,
        fonts:`${buildFolder}/fonts/`,
    },
    src: {
        html: `${srcFolder}/*.html`,
        scss: `${srcFolder}/style/style.scss`,
        js: `${srcFolder}/js/index.js`,
        images: `${srcFolder}/image/*.{jpg,jpeg,png,gif,webp}`,
        svg:`${srcFolder}/image/*.svg`,
        svgicons:`${srcFolder}/svgicons/*.svg`,
    },
    watch: {
        html: `${srcFolder}/**/*.html`,
        scss: `${srcFolder}/style/*.scss`,
        js: `${srcFolder}/js/*.js`,
        images: `${srcFolder}/image/*.{jpg,jpeg,png,gif,webp,svg,ico}`,
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
    ftp: ``,
}