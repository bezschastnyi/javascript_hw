'use strict'

const svgImg1 = document.querySelector('.svg_img');
const svgImg2 = document.querySelector('.svg_img1');
const menuSize = document.querySelector('.nav-bar__menu');

function showIconMenu(icon, iconSecond, menu) {
    icon.addEventListener('click', e => {
            iconSecond.classList.toggle('svg-disabled');
            icon.classList.toggle('svg-disabled');
            menu.classList.toggle('show_menu')
        }
    )
    iconSecond.addEventListener('click', e => {
            icon.classList.toggle('svg-disabled');
            iconSecond.classList.toggle('svg-disabled');
            menu.classList.toggle('show_menu')
        }
    )
}

showIconMenu(svgImg1, svgImg2, menuSize);