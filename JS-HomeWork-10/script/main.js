'use strict'

let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.data-content');

[...tabsTitle].forEach(tab => addEventListener('click', showTabs))

function showTabs(event) {
    let dataTabId = event.target.dataset.id;
    [...tabsTitle].forEach((tab, i) => {
        tab.classList.remove('active');
        tabsContent[i].classList.remove('active');
    })

    tabsTitle[dataTabId].classList.add('active');
    tabsContent[dataTabId].classList.add('active');
}

