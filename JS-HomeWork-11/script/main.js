'use strict'
const form = document.querySelector('.password-form');
const passwordInput = document.querySelector('.pass1');
const passwordInput2 = document.querySelector('.pass2');
const showPassword = document.querySelector('.icon-password1');
const showPassword2 = document.querySelector('.icon-password2');
const showPassword3 = document.querySelector('.icon-password3');
const showPassword4 = document.querySelector('.icon-password4');

function showIcon(icon, iconSecond) {
    icon.addEventListener('click', e => {
        iconSecond.classList.remove('show');
        icon.classList.add('show');
    }
    )
    iconSecond.addEventListener('click', e => {
        icon.classList.remove('show');
        iconSecond.classList.add('show');
    }
    )
}

showIcon(showPassword, showPassword3);
showIcon(showPassword2, showPassword4);

function showPass(password, icon) {
    icon.addEventListener('click', e => {
        if (password.getAttribute('type') === 'password') {
            password.setAttribute('type', 'text');
            showIcon(showPassword, showPassword3);
            showIcon(showPassword2, showPassword4);
        } else {
            password.setAttribute('type', 'password');
            showIcon(showPassword, showPassword3);
            showIcon(showPassword2, showPassword4);
        }
    }
    )
}

showPass(passwordInput, showPassword);
showPass(passwordInput, showPassword3);
showPass(passwordInput2, showPassword2);
showPass(passwordInput2, showPassword4);

const p = document.createElement('p');
p.className = "alert";
form.addEventListener('submit', e => {
    e.preventDefault();
    if (passwordInput.value === passwordInput2.value) {
        p.remove();
        alert('You are welcome');
    } else {
        p.innerText = "Потрібно ввести однакові значення";
        passwordInput2.after(p);
    }
}
)




