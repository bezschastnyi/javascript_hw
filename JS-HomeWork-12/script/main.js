
//Чому для роботи з input не рекомендується використовувати клавіатуру?

// Потому как для ввода данных в input используется не только клавиатура, а копирование
// и вставка, голосовое управление и т.д. И отследить событяи не всегда возможно.

'use strict'
const btn = document.querySelectorAll('.btn');

window.addEventListener("keydown", function (e) {
    let keys = e.code[e.code.length - 1]
    if (e.code === "Enter") {
        document.querySelector('.Enter').classList.add('changeColor')
    } else {
        document.querySelector('.Enter').classList.remove('changeColor')
        for (let i=0; i < btn.length; i++)
            if (btn[i].textContent === keys) {
                btn[i].style.backgroundColor="#0000F0"
            } else btn[i].style.backgroundColor="#000000"
}
}
)
