//Отвееты на теоритические вопросы

//Опишіть своїми словами різницю між функціями setTimeout() і setInterval()
// setTimeout() работает один раз через заданное время, setInterval() через заданный интервал срабатывает каждый раз.

//Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//Она стработает сразу, без задержек, как только весь код до этой функции выполниться.

//Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//Потому как он останется в памяти.


'use strict'

const images = document.querySelectorAll('.image-to-show');
let count = 0;
let timer;
const pauseBtn = document.createElement("button");
document.body.append(pauseBtn);
pauseBtn.innerHTML = 'Припинити';
pauseBtn.style.marginLeft = '410px';
pauseBtn.style.marginTop = '20px';
const returnBtn = document.createElement("button");
document.body.append(returnBtn);
returnBtn.innerHTML = 'Відновити показ';
returnBtn.style.marginLeft = '320px';

show();

function show() {
    for (let i = 0; i < images.length; i++) {
        images[i].classList.add('show');
    }
    images[count].classList.remove('show');
    if (count + 1 === images.length) {
        count = 0;
    } else {
        count++;
    }
    timer = setTimeout(show, 3000);
}

pauseBtn.addEventListener('click', () => {
    clearInterval(timer)
    countMS.classList.add('show');
})

returnBtn.addEventListener('click', () => {
    setTimeout(show, 3000);
    timerSms(0, 1)
    countMS.classList.remove('show');
})

let countMS = document.querySelector('.countMS');

function timerSms(COUNT_MS, COUNT) {
    setInterval(() => {
        countMS.innerHTML = `До зміни картники залишилось - ${3 - COUNT}:${100 - COUNT_MS}`;
        if (COUNT_MS === 99) {
            COUNT++;
            COUNT_MS = 0;
        }
        COUNT_MS++;
        if (COUNT > 3) {
            COUNT = 1
        }
    }, 10);
}

timerSms(0, 1);



