//ответы на теоритические вопросы

//1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

// Дерево элементов, или по другому структура страницы, с которой мы привыкли работать
// в среде разработчика в браузере. Зная структуру и понимая ее можно достучаться до
// любого элнмента страницы, создать новый или удалить старый.


//2. Какая разница между свойствами HTML-элементов innerHTML и innerText?

//innerHTML анализирует полученный контент и интерпретирует теги, innerText будет читать все как текст


//Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее?
// 6 способов: getElementsByTagName, getElementById, querySelector, querySelectorAll, getElementsByClass
//предпочтительно querySelector, querySelectorAll.

'use strict'

const paragraph = document.getElementsByTagName('p');
Array.from(paragraph).forEach(elem => elem.style.backgroundColor = '#ff0000');

const elementWithId = document.getElementById('optionsList');
console.log(elementWithId);
const parentElem = elementWithId.parentElement;
console.log(parentElem);

const childNodes = elementWithId.childNodes;
Array.from(childNodes).forEach(elem => console.log(`Имя нода: ${elem.nodeName} Тип нода: ${elem.nodeType}`));

document.getElementById('testParagraph').innerHTML = '<p>This is a paragraph</p>';

const getElem = document.querySelector('.main-header').querySelectorAll('li');
console.log(getElem);
Array.from(getElem).forEach(elem => elem.classList.add('nav-item'));
console.log(getElem)

const getElem2 = document.querySelectorAll('.section-title');
Array.from(getElem2).forEach(elem => elem.classList.remove('section-title'));




