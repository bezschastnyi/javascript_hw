'use strict'

const massive = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const newElement = document.createElement("ul");
function showList (arr, elem) {
    for (const item in arr) {
        const li = document.createElement("li");
        li.textContent = arr[item];
        elem.append(li);
    }
    document.body.appendChild(newElement);
}

showList (massive, newElement);
    
let time;
let timeToEnd = 3;

function countTimer() {
    document.querySelector('.timer').innerHTML = `До очищения страницы ${timeToEnd} секунд`;
    timeToEnd--;
    if (timeToEnd < 0) {
        clearTimeout(time);
        newElement.remove()
    }
    else {
       return time = setTimeout(countTimer, 1000);

    }
}
countTimer();