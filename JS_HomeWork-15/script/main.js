//Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?

// Рекурсия - это функция, которая вызывает сама себя и продолжает вычисления, пока не будет выполнено базовое условие.

let number;

number = Number(prompt('Введите число'));

function countFactorial (number) {
    if (number !== 1 && number !== 0) {
        return number * countFactorial(number - 1);
    } else return number;
}

console.log(countFactorial(number))