'use strict'
const tabsLabel = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');
const tabsContentItems = [...tabsContent.children];

function showTabs(collectionOne, collectionTwo) {

    collectionOne.addEventListener('click', e => {
            const target = e.target;

            if (target !== collectionOne) {
                [...collectionOne.children].forEach(item => {
                    item.classList.remove('active');
                });

                collectionTwo.forEach(tab => {
                    if (tab.dataset.id === target.dataset.id) {
                        tab.classList.add('active');
                    } else {
                        tab.classList.remove('active');
                    }
                });
                target.classList.toggle('active');
            }
        }
    )
}

showTabs(tabsLabel, tabsContentItems);
const tabsLabel2 = document.querySelector('.tab_page3');
const tabsContent2 = document.querySelector('.content_page3');
const tabsContentItems2 = [...tabsContent2.children];
showTabs(tabsLabel2, tabsContentItems2);

const pictureLoad = document.querySelector('.btn_loadMore');
const pictureLoadShow = document.querySelector('.load_more');
const pictureLoadShow2 = document.querySelector('.load_more1');
pictureLoad.addEventListener('click', e => {
    pictureLoadShow.style.display = 'block';
    pictureLoad.addEventListener('click', e => {
        pictureLoadShow2.style.display = 'block';
        pictureLoad.style.display = 'none';
    })
})


const urlImageGraphicDesign = ["./image/graphic_design/graphic-design1.jpg", "./image/web_design/web-design2.jpg", "./image/graphic_design/graphic-design3.jpg",
    "./image/graphic_design/graphic-design4.jpg", "./image/graphic_design/graphic-design5.jpg", "./image/graphic_design/graphic-design6.jpg",
    "./image/graphic_design/graphic-design7.jpg", "./image/graphic_design/graphic-design8.jpg", "./image/graphic_design/graphic-design9.jpg",
    "./image/graphic_design/graphic-design10.jpg", "./image/graphic_design/graphic-design11.jpg", "./image/graphic_design/graphic-design12.jpg",]

const urlImageLanding = ["./image/landing_page/landing-page1.jpg", "./image/landing_page/landing-page2.jpg",
    "./image/landing_page/landing-page3.jpg", "./image/landing_page/landing-page4.jpg",
    "./image/landing_page/landing-page5.jpg", "./image/landing_page/landing-page6.jpg",
    "./image/landing_page/landing-page7.jpg"];

const urlImageWebDesign = ["./image/web_design/web-design1.jpg", "./image/web_design/web-design2.jpg",
    "./image/web_design/web-design3.jpg", "./image/web_design/web-design4.jpg",
    "./image/web_design/web-design5.jpg", "./image/web_design/web-design6.jpg",
    "./image/web_design/web-design7.jpg"];

const urlImageWordpress = ["./image/wordpress/wordpress1.jpg", "./image/wordpress/wordpress2.jpg",
    "./image/wordpress/wordpress3.jpg", "./image/wordpress/wordpress4.jpg",
    "./image/wordpress/wordpress5.jpg", "./image/wordpress/wordpress6.jpg",
    "./image/wordpress/wordpress7.jpg", "./image/wordpress/wordpress8.jpg",
    "./image/wordpress/wordpress9.jpg", "./image/wordpress/wordpress10.jpg"];

const urlImageLanding2 = ["./image/landing_page/landing-page1.jpg", "./image/landing_page/landing-page2.jpg",
    "./image/landing_page/landing-page3.jpg", "./image/landing_page/landing-page4.jpg",
    "./image/landing_page/landing-page5.jpg"];

let tabs1 = document.querySelector('.image1');
let tabs2 = document.querySelector('.image2');
let tabs3 = document.querySelector('.image3');
let tabs4 = document.querySelector('.image4');
let tabs5 = document.querySelector('.image5');
let tabs6 = document.querySelector('.image6');
let tabs7 = document.querySelector('.image7');

function createDivImage(collection, tab, title) {

    collection.forEach(item => {
        let newDivHover = document.createElement('div');
        newDivHover.classList.add('portfolio-gallery-card');
        newDivHover.setAttribute('data-content', title);
        tab.appendChild(newDivHover);
        let newDiv = document.createElement('img');
        newDiv.classList.add('portfolio-gallery-img');
        newDiv.setAttribute('src', item);
        newDivHover.appendChild(newDiv);
        let div2 = document.createElement('div');
        div2.classList.add('img-card');
        newDivHover.appendChild(div2);
        let div3 = document.createElement('div');
        div3.classList.add('img-card-icon-wrap');
        div2.appendChild(div3);
        let elem4 = document.createElement('span');
        div3.appendChild(elem4);
        let elem5 = document.createElement('i');
        elem5.classList.add('img-card-icon');
        elem5.classList.add('fas');
        elem5.classList.add('fa-link');
        elem4.appendChild(elem5);
        let elem6 = document.createElement('span');
        div3.appendChild(elem6);
        let elem7 = document.createElement('i');
        elem7.classList.add('img-card-icon');
        elem7.classList.add('fas');
        elem7.classList.add('fa-square');
        elem6.appendChild(elem7);
        let elem8 = document.createElement('p');
        elem8.innerText = 'creative design';
        elem8.classList.add('img-card-title');
        div3.appendChild(elem8);
        let elem9 = document.createElement('p');
        elem9.innerText = title;
        elem9.classList.add('img-card-category');
        div3.appendChild(elem9);
    })
}

const categorGD = 'Graphic Design';
createDivImage(urlImageGraphicDesign, tabs4, categorGD);
const categorWD = 'Web Design';
createDivImage(urlImageWebDesign, tabs5, categorWD);
const categorLan = 'Landing page';
createDivImage(urlImageLanding, tabs6, categorLan);
const categorWpr = 'Wordpress';
createDivImage(urlImageWordpress, tabs7, categorWpr);
createDivImage(urlImageWebDesign, tabs1, categorWD);
createDivImage(urlImageLanding2, tabs1, categorLan);
createDivImage(urlImageGraphicDesign, tabs2, categorGD);
createDivImage(urlImageWordpress, tabs3, categorWpr);
createDivImage(urlImageWebDesign, tabs3, categorWD);

/*=====SLIDER=====*/

let currentSlide = 0;
const navigation = document.querySelectorAll('.slider-user-foto-small');
const slides = document.querySelectorAll('.slider-user-review');
const next = document.getElementById('arrowRight');
const previous = document.getElementById('arrowLeft');

for (let i = 0; i < navigation.length; i++) {
    navigation[i].onclick = function () {
        currentSlide = i;
        document.querySelector('.slider-user-review.container.active').classList.remove('active');
        document.querySelector('.slider-user-foto-small.active').classList.remove('active');
        navigation[currentSlide].classList.add('active');
        slides[currentSlide].classList.add('active');
    }
}

next.onclick = function () {
    nextSlide(currentSlide);
};

previous.onclick = function () {
    previousSlide(currentSlide);
};

function nextSlide() {
    goToSlide(currentSlide + 1);
}

function previousSlide() {
    goToSlide(currentSlide - 1);
}

function goToSlide(n) {
    hideSlides();
    currentSlide = (n + slides.length) % slides.length;
    showSlides();
}

function hideSlides() {
    slides[currentSlide].className = 'slider-user-review container';
    navigation[currentSlide].className = 'slider-user-foto-small';
}

function showSlides() {
    slides[currentSlide].className = 'slider-user-review container active';
    navigation[currentSlide].className = 'slider-user-foto-small active';
}


